import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Navbar from './components/Navbar';
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Post from './components/Post';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <header className="App-header">
        <Navbar />
        <Switch> {/*due to switch method router only take one path at a time*/}
          <Route exact path='/' component={Home} /> {/* exact used to make home path unique */}
          <Route path='/about' component={About} />
          <Route path='/contact' component={Contact} />
          <Route path='/:post_id' component={Post} />
        </Switch>
      </header>
      </BrowserRouter>
    </div>
  );
}

export default App;
