import React, { Component } from 'react';
// import axios from 'axios'
import {Link} from 'react-router-dom';
import pokeball from '../pokeball.png'
import { connect } from 'react-redux';

class Home extends Component {
  // state = {
  //   posts: []
  // }
  // componentDidMount() {
  //   axios.get('https://jsonplaceholder.typicode.com/posts').then(res => {
  //     this.setState({
  //       posts: res.data
  //     })
  //   })
  // }
  render() {
    // console.log(this.props.posts)
    const { posts } = this.props
    const postList = posts.length?(
      posts.map(post =>{
        return (
          <div className="post card" key={post.id}>
            <img src={pokeball} alt="A pokeball"/>
            <div className="card-content">
              <Link to={'/'+ post.id}>
              <span className="card-title red-text">{post.title}</span>
              </Link>
              <p>{post.body}</p>
            </div>
          </div>
        )
      })
    ):
    (
      <div className="container">No posts yet !</div>
    );
  return (
    <div className="container home">
      <h3 className="center">Home</h3>
      {postList}
    </div>
  )
  }
}

const mapStateToProps = (state) => {
  return {
    posts: state.posts
  }
}

export default  connect(mapStateToProps)(Home); // connect method will communicate with redux to fetch state