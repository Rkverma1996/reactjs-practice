import React from 'react';
import { Link, NavLink, withRouter } from 'react-router-dom';

const Navbar = (props) => {
  console.log(props)
  // setTimeout(
  //   () => {
  //     props.history.push('/about');
  //   }, 2000
  // )
  return (
    <nav className="nav-wrapper red darken-3">
    <div className="container">
      <a className="brand-logo">Poke's Times</a>
      <ul id="nav-mobile" className="right hide-on-med-and-down">
        <li><Link to="/">Home</Link></li>
        <li><NavLink to="/contact">Contact</NavLink></li>
        <li><NavLink to="/about">About</NavLink></li> {/* NavLink used bcs when we click on this tab then an active class will generate by this */}
      </ul>
    </div>
  </nav>
  )
};
export default withRouter(Navbar);