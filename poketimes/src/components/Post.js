import React, { Component } from 'react'
import axios from 'axios';

class Post extends Component {
  state = {
    post: null
  }
  componentDidMount() {
    // console.log(this.props)
    let id = this.props.match.params.post_id
    axios.get('https://jsonplaceholder.typicode.com/posts/'+id).then( post => {
    this.setState({
      post: post.data
    })
  })
  }
  render() {
    const post = this.state.post ? (
      <div className="post">
        <h4 className="center">{this.state.post.title}</h4>
      </div>
    ):(
      <h4 className="center">Loading......</h4>
    );
    return (
      <div className="container">
        {post}
      </div>
    )
  }
}

export default Post;