import React, { Component } from 'react';
import Rohit from './rohit'
import AddRohit from './addRohit'
import './rohit.css'

class App extends Component {
  state = {
    rohit : [
      { name: 'Rohit', age: 23 , belt: 'black', id: 1 },
      { name: 'Sanjeev', age: 24, belt: 'red', id: 2 },
      { name: 'Rocky', age: 25, belt: 'green', id: 3 },
      { name: 'Revel', age: 26, belt: 'yellow', id: 4 },
    ]
  }

  addRohit =(rohit) => {
    rohit.id = Math.random();
    let mohit = [...this.state.rohit, rohit];
    this.setState({
      rohit: mohit
    })
  }

  deleteRohit = (id) => {
    // console.log(id)
    let rohit = this.state.rohit.filter(mohit => {
      return mohit.id !== id
    });
    this.setState({
      rohit: rohit
    });
  }
  componentDidMount() {
    console.log('component did mounted');
  }

  componentDidUpdate(prevProps, prevState ) {
    console.log('component Updated');
    console.log(prevState);
  }
  render() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>hllo react js user</h1>
      </header>
      <Rohit rohit={this.state.rohit } deleteRohit = {this.deleteRohit} /> {/* Added props in form of state */}

      <AddRohit addRohit={this.addRohit}></AddRohit>
    </div>
  );
  }
}

export default App;
