import React from 'react';

const Rohit = ({rohit, deleteRohit}) => {
    console.log('out put is ', rohit)
    // const { rohit } = rohit;

    const rohitList = rohit.map( rk => {
      return rk.age > 20 ? (
        <div className="rk" key={ rk.id }>
          <div>
            Name: { rk.name }
          </div>
          <div>
            Age: { rk.age }
          </div>
          <div>
            Belt: { rk.belt }
          </div>
          <button onClick={() => { deleteRohit(rk.id) }}>
            Delete Info
          </button>
        </div>
      ): null;
    })
    return(
      <div className="Rohit">
        <div>
        { rohitList }
        </div>
      </div>
    )
  }

export default Rohit