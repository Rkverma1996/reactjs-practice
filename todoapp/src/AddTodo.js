import React, { Component } from 'react'

class AddTodo extends Component {
  state = {
    content: ''
  }
  handleChange = (e) => {
    // console.log(e.target);
    this.setState ({
      content: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    // console.log(this.props.addTodo);
    this.props.addTodo(this.state)
    this.setState({
      content: ''
    })
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>Add Todo:</label>
          <input type='text' onChange={this.handleChange} value={this.state.content} />
          <button className="btn btn-small">Add</button>
        </form>
      </div>
    )
  }
}

export default AddTodo;